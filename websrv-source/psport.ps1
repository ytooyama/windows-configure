New-NetFirewallRule -DisplayName "Allow apache Port" -Direction Inbound -Protocol TCP -LocalPort 80 -Action allow
New-NetFirewallRule -DisplayName "Allow apache Port" -Direction Inbound -Protocol TCP -LocalPort 443 -Action allow
New-NetFirewallRule -DisplayName "Allow Zabbix Port" -Direction Inbound -Protocol TCP -LocalPort 10050 -Action allow
