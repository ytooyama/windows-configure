﻿$MyINT='Ethernet0'

$MyIP='172.17.14.159'
$MyGW='172.17.14.1'
$MyNS='172.17.14.3'
$MyPrefix='24'

#Configure
 #Remove IP
 Remove-NetIPAddress -InterfaceAlias $MyINT
 #Set IP
 New-NetIPAddress -InterfaceAlias $MyINT -IPAddress $MyIP -PrefixLength $MyPrefix -DefaultGateway $MyGW -AddressFamily IPv4
 Set-DnsClientServerAddress -InterfaceAlias $MyINT -ServerAddresses $MyNS
 #Check the IPs
 Get-NetIPAddress -InterfaceAlias $MyINT
 Get-DnsClientServerAddress -InterfaceAlias $MyINT