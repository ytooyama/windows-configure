# Windows-Configure

(Last Update: 2016/03/03)

Windows Serverの構成管理をAnsibleやPowerShellを使って行うのが目標。
このリポジトリーにはそれに必要なYAMLファイルとPowerShellスクリプトなどが置かれています。

## 事前に行うこと

AnsibleでWindowsを操作するには事前に[ConfigureRemotingForAnsible.ps1](https://github.com/ansible/ansible/blob/devel/examples/scripts/ConfigureRemotingForAnsible.ps1)を使って、Windowsに設定変更が必要です。以下をps1ファイルとして保存して実行すると、ダウンロードから実行まで自動化できます。

```
#download
Invoke-WebRequest -Uri "https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1" -OutFile "ConfigureRemotingForAnsible.ps1"
#configure
powershell -ExecutionPolicy RemoteSigned .\ConfigureRemotingForAnsible.ps1
```

「ConfigureRemotingForAnsible.ps1」はAnsible公式で配布する
WindowsをAnsibleでリモート制御するために必要な設定を自動化するスクリプトです。
以下で開発されています。

- <https://github.com/ansible/ansible/blob/devel/examples/scripts/ConfigureRemotingForAnsible.ps1>


##利用までの流れ

- Ansibleをインストール
- Ansibleでデプロイする対象と認証情報をhostsに記述
- Ansibleで行う作業をyamlファイルに記述
- `ansible-playbook`コマンドで実行

実行例

```
$ ansible-playbook -i hosts remote-ps.yml
(通常の実行方法)
$ ansible-playbook -i hosts remote-ps.yml -vvv
(詳細にログ出力したい場合)
```

##ファイルについて

ファイル一式  | 内容
------------- | -------------
ansible-yaml-sample| Ansible hostsとYAMLファイルのサンプル。
powershell-sample  | PowerShellのスクリプト。Ansibleでこのファイルを呼び出して、リモートで実行する。
websrv-source      | Ansibleホストと対象のWindowsサーバーと「IP reachable」なWebサーバー上に置くファイル。Ansibleスクリプトで対象のWindowsサーバーにダウンロードして必要な処理を実行するためのもの。

YAMLファイルでは「[ansible/ansible-module-core](https://github.com/ansible/ansible-modules-core)」 モジュールを利用しています。